import pandas as pd
import re
import xlsx2csv as x2csv
import datetime
import os
from  copy import deepcopy

#import model_functions

dataD='Cannes_Lions_tim1/xls'
os.chdir('/Users/charlotteszostek/Documents/CannesLion/' + dataD + '/')


def main():    
    ### all files
    SHORTLISTS=[]
    WINNERS=[]
    CORE=pd.DataFrame([])
    NONCORE=pd.DataFrame([])
    DATA=[]
    F=[]
    for (dirpaths, dirnames, filenames) in os.walk('.'):
        F.extend(filenames)
            
        
   # for f in F:
   #     if re.findall(r'Shortlist',f):
   #         name=f[0:-4]+'csv'
   #         x2csv.Xlsx2csv(f).convert(open(f[0:-4]+'csv', "w+"))
   #         SHORTLISTS.append(name)
   #         
   #     if re.findall(r'Winner',f):
   #         #x2csv.Xlsx2csv(f).convert(open(f[0:-4]+'csv', "w+"))
   #         WINNERS.append(name)
   #     
   #
   
    for f in F:
       if re.findall(r'.csv',f):
           DATA.append(f)
           if re.findall(r'Shortlist',f):
            SHORTLISTS.append(f) 
           if re.findall(r'Winner',f):
             WINNERS.append(f)         
    
    

    
    ### all from /SHORTLISTS
    ### all from /WINNERS
    CORE=pd.DataFrame([])
    NONCORE=pd.DataFrame([])

    
    #for short in SHORTLISTS:
    for short in WINNERS:    
        print short
    
        award='s'
        year=re.findall(r'\d{4}',short)
        category=re.findall(r'[A-Z][a-z]*_Lions',short)[1]
        subcategory=''

        a=pd.read_csv(short)
        if 'TITLE' not in a.columns[0]:
            if 'Title' not in a.columns[0]:
                a.to_csv('dummy.csv', header = False)
                a=pd.read_csv('dummy.csv')
        if len(a.columns)!=5:
            a=a[a.columns[0:5]]   
            print short     
            print a.columns
            
        a.columns=['TITLE','CLIENT','PRODUCT','ENTRACT_COMPANY','COUNTRY']
        a=a.dropna(how='all')
        
        v=a.ix[a.index[0]]
        if v.isnull().sum()>0:
                    if v.isnull().sum()==4:
                        for cell in v:
                            if type(cell)!=float:       
                                
                                              
                                if re.findall(r'[A-Z]{1}\d{2}',cell):
                                    subcategory=re.findall(r'[A-Z]{1}\d{2}',cell)
                                    a=a.drop(a.index[0])
        
        
        flag=False
        
        I1=deepcopy(a.index[1:])
        I2=deepcopy(a.index[0:-1])
        
        #for row,above in zip(a.index[1:],a.index[0:-1]):
        index=I1[0]
        
        SUB=pd.DataFrame([])
        first=True
        while len(I1)>0: 
            
            row=I1[0]
            above=I2[0]
                        
            r=a.ix[row]
            v=a.ix[above]
                    
            if r.isnull().sum()>0:
            # first iteration

                for cell in r:
                    if type(cell)!=float:              

                        if re.findall(r'[A-Z]{1}\d{2}',cell):
                            subcategory=re.findall(r'[A-Z]{1}\d{2}',cell)
                            flag=True 
                            a=a.drop(row)    
                            I2=I2.drop(I2[where(I2==row)])
                            I1=I1.drop(I1[where(I1==row)])             
                        if re.findall(r'Gold|Silver|Bronze|Grand',cell):
                            award=re.findall(r'Gold|Slber|Bronze|Grand',cell)
                            flag=True
                            a=a.drop(row)
                            
                            
                            I2=I2.drop(I2[where(I2==row)])
                            I1=I1.drop(I1[where(I1==row)]) 
            
                if flag is False:
                   new=[]
                   for z,zz in zip(v,r): 
                    if  type(z) is float or type(zz) is float:
                       if type(z) is float:
                        new.append(zz)
                       else:
                        new.append(z)   
                    if type(z)is str and type(zz) is str:
                         new.append(z+ ' ' + zz)
                   a.loc[above]=new
                   a=a.drop(row)
                   I2=I2.drop(I2[where(I2==row)])
                   I1=I1.drop(I1[where(I1==row)])


            data=[award,subcategory,category,year]   
                # no deletion
            if r.isnull().sum()==0:
                I1=I1.drop(I1[0])
                I2=I2.drop(I2[0])
                sub=pd.DataFrame([data])
                sub.columns=['award','subcategory','category','year']
                SUB=SUB.append(sub)
            
            flag=False
            if first==True:
                sub=pd.DataFrame([data])
                sub.columns=['award','subcategory','category','year']
                SUB=SUB.append(sub)
            first=False    
        print len(a)
        print len(sub)

            
            
        CORE=CORE.append(a)
        NONCORE=NONCORE.append(SUB)
            
        CORE.index=np.arange(0,len(CORE))
        NONCORE.index=np.arange(0,len(NONCORE))
        LIONS_2=pd.concat([CORE, NONCORE], axis=1, join='inner')
        LIONS_2.to_csv('Lions2.csv')
