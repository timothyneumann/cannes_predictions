from pymongo import MongoClient
import csv
from datetime import datetime
from pprint import PrettyPrinter
import re
pp = PrettyPrinter(indent=1)
shortlist_col = MongoClient('localhost')['app-nest-cannes']['shortlist']
shortlist_bulk = shortlist_col.initialize_ordered_bulk_op()
day1 = ['Mobile_Lions', 'Press_Lions']
day2 = ['Direct_Lions', 'Media_Lions', 'Outdoor_Lions', 'R_Lions', 'Activation_Lions', 'Effectiveness_Lions'] + day1
day3 = ['Design_Lions', 'Radio_Lions', 'Cyber_Lions'] + day2
day4 = ['Film_Lions', 'Craft_Lions', 'Entertainment_Lions', 'Integrated_Lions'] + day3

def _headers(row):
    headers = {}
    for ind, el in enumerate(row):
        headers[el.lower()] = ind
    return headers
date = datetime.utcnow()
data_hash = {}
entrant_company_hash = {}
headers = {}
with open('Lions_shortlists.csv', 'rU') as finone:
    readerone = csv.reader(finone, delimiter=',', quotechar='"')
    for line_number, data in enumerate(readerone):
        if line_number == 0:
            headers = _headers(data)
        elif data[headers['category']] != '' and  data[headers['title']] != '' and data[headers['entract_company']] != '' and data[headers['year']] != '':
            year =  re.sub("\['|'\]", '', data[headers['year']])
            _id = data[headers['title']] + data[headers['entract_company']] + year + data[headers['category']]
            entrant_company_id = data[headers['entract_company']] + year + data[headers['category']]

            if entrant_company_id in entrant_company_hash:
                entrant_company_hash[entrant_company_id] += 1
            else:
                entrant_company_hash[entrant_company_id] = 1

            if _id in data_hash: 
                data_hash[_id]['category_nominations'] += 1
            else:
                obj = {
                    "nominee": {
                        "title": data[headers['title']],
                        "client": data[headers['client']],
                        "product": data[headers['product']],
                        "entrant_company": data[headers['entract_company']],
                        "entrant_company_category_nominations": 1,
                        "country": data[headers['country']]
                    },
                    "odds": 0,
                    "award": 'Grand Prix',
                    "category": data[headers['category']],
                    "category_nominations": 1,
                    "result": False,
                    "year": year,
                    "updated_at": date
                }
                data_hash[_id] = obj
for entry in data_hash:
    _entry = data_hash[entry]
    entrant_company_id = _entry['nominee']['entrant_company'] + _entry['year'] + _entry['category']
    _entry['nominee']['entrant_company_category_nominations'] = entrant_company_hash[entrant_company_id]

count = 0
for entry in data_hash:
    for entry_field in data_hash[entry]:
        if entry_field =='nominee':
            for field in data_hash[entry][entry_field]:
                if type(data_hash[entry][entry_field][field]) == str:
                    data_hash[entry][entry_field][field] = unicode(data_hash[entry][entry_field][field], errors = 'replace')
        else:
            if type(data_hash[entry][entry_field]) == str:
                data_hash[entry][entry_field] = unicode(data_hash[entry][entry_field], errors='replace')
    shortlist_bulk.insert(data_hash[entry])  
shortlist_bulk.execute()
