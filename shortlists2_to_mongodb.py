from pymongo import MongoClient
import csv
from datetime import datetime
from pprint import PrettyPrinter
import re
pp = PrettyPrinter(indent=1)
shortlist_col = MongoClient('localhost')['app-nest-cannes']['shortlist']
shortlist_bulk = shortlist_col.initialize_ordered_bulk_op()
day1 = ['Mobile_Lions', 'Press_Lions']
day2 = ['Direct_Lions', 'Media_Lions', 'Outdoor_Lions', 'R_Lions', 'Activation_Lions', 'Effectiveness_Lions'] + day1
day3 = ['Design_Lions', 'Radio_Lions', 'Cyber_Lions'] + day2
day4 = ['Film_Lions', 'Craft_Lions', 'Entertainment_Lions', 'Integrated_Lions'] + day3
files = [
'direct_lions_2015.csv',
'mobile_lions_2015.csv',
'press_lions_2015.csv',
'promo_and_activation_2015.csv',

]
category = {
    'Creative_Effectiveness_Lions_2014_Shortlist.csv':'Effectiveness_Lions',
'Cyber_Lions_2014_Shortlist.csv':'Cyber_Lions',
'Design_Lions_2014_Shortlist.csv':'Design_Lions',
'direct_lions_2015.csv':'Direct_Lions',
'Film_Craft_Shortlist.csv':'Craft_Lions',
'Film_Lions__Shortlist.csv':'Film_Lions',
'Media_Lions_2014_Shortlist.csv':'Media_Lions',
'mobile_lions_2015.csv':'Mobile_Lions',
'Outdoor_Lions_2014_Shortlist.csv':'Outdoor_Lions',
'press_lions_2015.csv':'Press_Lions',
'PR_Lions_2014_Shortlist.csv':'R_Lions',
'promo_and_activation_2015.csv':'Activation_Lions',
'Radio_Lions_2014_Shortlist.csv':'Radio_Lions'
}
def _headers(row):
    headers = {}
    for ind, el in enumerate(row):
        headers[el] = ind
    return headers
date = datetime.utcnow()
data_hash = {}
entrant_company_hash = {}
headers = {}
for _file in files:
    print _file
    with open(_file, 'rU') as finone:
        readerone = csv.reader(finone, delimiter=',', quotechar='"')
        for line_number, data in enumerate(readerone):
            if line_number == 0:
                headers = _headers(data)
                print headers
            elif line_number > 0 and data[headers['Title']] != '' and data[headers['Entrant Company']] != '':
                # year =  re.sub("\['|'\]", '', data[headers['year']])
                year = '2015'
                _id = data[headers['Title']] + data[headers['Entrant Company']] + year + category[_file]
                entrant_company_id = data[headers['Entrant Company']] + year + category[_file]

                if entrant_company_id in entrant_company_hash:
                    entrant_company_hash[entrant_company_id] += 1
                else:
                    entrant_company_hash[entrant_company_id] = 1

                if _id in data_hash: 
                    data_hash[_id]['category_nominations'] += 1
                else:
                    obj = {
                        "nominee": {
                            "title": data[headers['Title']],
                            "client": data[headers['Client']],
                            "product": data[headers['Product']],
                            "entrant_company": data[headers['Entrant Company']],
                            "entrant_company_category_nominations": 1,
                            "country": ''
                            # "country": data[headers['Country']]
                        },
                        "odds": 0,
                        "award": 'Grand Prix',
                        "category": category[_file],
                        "category_nominations": 1,
                        "result": False,
                        "year": year,
                        "updated_at": date
                    }
                    data_hash[_id] = obj
for entry in data_hash:
    _entry = data_hash[entry]
    entrant_company_id = _entry['nominee']['entrant_company'] + _entry['year'] + _entry['category']
    _entry['nominee']['entrant_company_category_nominations'] = entrant_company_hash[entrant_company_id]

count = 0
for entry in data_hash:
    for entry_field in data_hash[entry]:
        if entry_field =='nominee':
            for field in data_hash[entry][entry_field]:
                if type(data_hash[entry][entry_field][field]) == str:
                    data_hash[entry][entry_field][field] = unicode(data_hash[entry][entry_field][field], errors='replace')
        else:
            if type(data_hash[entry][entry_field]) == str:
                data_hash[entry][entry_field] = unicode(data_hash[entry][entry_field], errors='replace')
    # pp.pprint(data_hash[entry])
    shortlist_bulk.insert(data_hash[entry])  
shortlist_bulk.execute()
