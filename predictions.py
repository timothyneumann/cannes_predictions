from __future__ import division
import numpy as np
import statsmodels.api as sm
from pymongo import MongoClient
import random
import math
import warnings

day1 = ['Mobile_Lions', 'Press_Lions', 'Direct_Lions', 'Activation_Lions']
day2 = ['Media_Lions', 'R_Lions', 'Outdoor_Lions', 'Effectiveness_Lions']
day3 = ['Design_Lions', 'Radio_Lions', 'Cyber_Lions']
day4 = ['Film_Lions', 'Craft_Lions', 'Entertainment_Lions', 'Integrated_Lions']


def get_data(actual_category, supporting_list_of_categories_without_results=[], supporting_list_of_categories_with_results=[]):
    '''
    this function puts together the 'training data'.  it will get all the nominees from all the years specified for a particular category. 
    then it will look to see if that nominee was a nominee for any of the supporting list categories.  then it will look to see
    if the nominee is in any of the supporting list nominees.  the function is buckets the winners and the losers seperately.
    '''
    shortlist_col = MongoClient('localhost')['app-nest-cannes']['shortlist']
    nominees = shortlist_col.find({'category': actual_category, 'year': {'$in': ['2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']}})
    exog_list = []
    endog_list = []
    winning_exog_list = []
    losing_exog_list = []
    winners = 0
    losers = 0
    for nominee in nominees:
        total_title = 0
        total_company = 0
        total_wins = 0
        company_noms = 0
        company_wins = 0
        sum_company_wins = 0
        if nominee['result']:
            winners += 1
            nom_exog = [nominee['category_nominations'], nominee['nominee']['entrant_company_category_nominations']]
            for cat in supporting_list_of_categories_without_results:
                sup_docs = shortlist_col.find_one({'year':nominee['year'], 'category':cat, 'nominee.entrant_company':nominee['nominee']['entrant_company'],'nominee.title': nominee['nominee']['title']})
                # company_noms += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}).count()
                for doc in shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}):
                    company_noms += 1
                    sum_company_wins += doc['category_nominations']
                if sup_docs:
                    # print 'found ', sup_docs
                    total_title += sup_docs['category_nominations']
                    total_company += sup_docs['nominee']['entrant_company_category_nominations']
                    nom_exog = nom_exog + [sup_docs['category_nominations'], sup_docs['nominee']['entrant_company_category_nominations']]
                    
                else:
                    nom_exog = nom_exog + [0,0]

            for cat in supporting_list_of_categories_with_results:
                # company_noms += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}).count()
                for doc in shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}):
                    company_noms += 1
                    sum_company_wins += doc['category_nominations']
                company_wins += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company'], 'result':True}).count()
                sup_docs = shortlist_col.find_one({'year':nominee['year'], 'category':cat, 'nominee.entrant_company':nominee['nominee']['entrant_company'],'nominee.title': nominee['nominee']['title']})
                if sup_docs:
                    # print 'found ', sup_docs
                    total_title += sup_docs['category_nominations']
                    total_company += sup_docs['nominee']['entrant_company_category_nominations']
                    res = 1 if sup_docs['result'] else  0
                    total_wins += res
                    nom_exog = nom_exog + [sup_docs['category_nominations'], sup_docs['nominee']['entrant_company_category_nominations'], res]
                    
                else:
                    nom_exog = nom_exog + [0,0,0]
            nom_exog = nom_exog + [total_title, total_company, total_wins, company_noms, company_wins, sum_company_wins]
            winning_exog_list.append(nom_exog)
        elif not nominee['result']:
            losers += 1
            nom_exog = [nominee['category_nominations'], nominee['nominee']['entrant_company_category_nominations']]
            for cat in supporting_list_of_categories_without_results:
                # company_noms += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}).count()
                for doc in shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}):
                    company_noms += 1
                    sum_company_wins += doc['category_nominations']
                sup_docs = shortlist_col.find_one({'year':nominee['year'], 'category':cat, 'nominee.entrant_company':nominee['nominee']['entrant_company'],'nominee.title': nominee['nominee']['title']})
                if sup_docs:
                    # print 'found ', sup_docs
                    total_title += sup_docs['category_nominations']
                    total_company += sup_docs['nominee']['entrant_company_category_nominations']
                    nom_exog = nom_exog + [sup_docs['category_nominations'], sup_docs['nominee']['entrant_company_category_nominations']]
                    
                else:
                    nom_exog = nom_exog + [0,0]

            for cat in supporting_list_of_categories_with_results:
                sup_docs = shortlist_col.find_one({'year':nominee['year'], 'category':cat, 'nominee.entrant_company':nominee['nominee']['entrant_company'],'nominee.title': nominee['nominee']['title']})
                # company_noms += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}).count()
                for doc in shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}):
                    company_noms += 1
                    sum_company_wins += doc['category_nominations']
                company_wins += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company'], 'result':True}).count()
                if sup_docs:
                    # print 'found ', sup_docs
                    total_title += sup_docs['category_nominations']
                    total_company += sup_docs['nominee']['entrant_company_category_nominations']
                    res = 1 if sup_docs['result'] else  0
                    total_wins += res
                    nom_exog = nom_exog + [sup_docs['category_nominations'], sup_docs['nominee']['entrant_company_category_nominations'], res]
                    
                else:
                    nom_exog = nom_exog + [0,0,0]
            nom_exog = nom_exog + [total_title, total_company, total_wins, company_noms, company_wins, sum_company_wins]
            losing_exog_list.append(nom_exog)

    return winners, losers, winning_exog_list, losing_exog_list


def odds(winners, losers, winning_exog_list, losing_exog_list, nominees):   
    '''
    this function takes the exogs from the get data function and gives odds to each of the nominees.
    it generates a random subset of losers from the losers exoglist, that is the same length as the 
    winning exog list.  it will then fit a logit model to this set of data.  then each of the nominees
    use these parameters to get a prediction for their odds of winning.  once this process is repeated a 
    bunch of times, for each nominee an average prediction is generated.  this is then their official odds
    of winning.
    '''
    results = []
    params = []
    count = 0
    
    # generate a random list of indices that has length equal to the amount of winners
    while count < 1000:
        random_list = []
        while len(random_list) < winners:
            r = int(math.floor(random.random() * (losers - 1) + 0.5))
            if r not in random_list:
                random_list.append(r)

        #now use the random indices to determine exog_list, which is really a matrix
        exog_list = winning_exog_list + [losing_exog_list[int(i)] for i in random_list]
        # print exog_list
        endog_list = [1 for i in range(winners)] + [0 for i in range(winners)]
        exog = np.array(exog_list)
        endog = np.array(endog_list)
        
        #if one wanted to use the MNLogit they could switch to it here (assuming it makes sense for the data)
        # mlogit_mod = sm.MNLogit(endog, exog)
        logit_mod = sm.Logit(endog, exog)
        # mlogit_res = mlogit_mod.fit(maxiter=100)
        
        #due to some of the nominee losers being so similar to the winners, the fit will sometimes fail to 
        #converge, which emits a warning.  I handle this warning as an error and generate a new subset
        #of losers to look at so that I can hopefully get a fit that converges.  this process repeates until 
        # the 'count', number of predictions, exceeds a certain limit (it is 1000 right now)  when a convergent fit
        #comes through then each nominee uses the returned parameters from the fit to determine their odds.
        #these are kept track of in the nominee list.
        warnings.filterwarnings('error')   
        try:
            logit_res = logit_mod.fit(maxiter=1000)
            count += 1
            # print logit_res.params
            params.append(logit_res.params)
            for nominee in nominees:
                prediction = logit_mod.predict(logit_res.params, np.array(nominee[1]))
                nominee[2].append(prediction)
        except Exception, e:
            # print count
            # print str(e)
            pass
    
    #for each nominee the average of all their odds predictions are determined, which determines their official odds
    params = np.array(params)
    # print params
    print 'parameters', np.mean(params, axis=0)
    for nominee in nominees:
        nominee[0]['odds'] = sum(nominee[2]) / len(nominee[2])
    return nominees

def main():
    '''
    the main function is used to gather the information on the current nominees, in order to use this information
    in the odds determination. this is where one specifies what other category information they would like to use.
    '''
    #where the predictions go once determined
    predictions_col = MongoClient('localhost')['app-nest-cannes']['awards']
    
    #where the training data is stored
    shortlist_col = MongoClient('localhost')['app-nest-cannes']['shortlist']
    
    #specify the category of interest in the next line
    category = 'Cyber_Lions'
    nominees = shortlist_col.find({'category': category, 'year': '2014'})
    
    #specifiy the cateogories you would like to use that will also include win / loss information
    supporting_list_of_categories_with_results = day1 + day2 
    
    #specifiy the categories to use that will just use nomination information
    supporting_list_of_categories_without_results = ['Design_Lions', 'Radio_Lions']
    
    #get the training data for the category of interest
    res = get_data(category, supporting_list_of_categories_without_results, supporting_list_of_categories_with_results)
    predict_nominees = []
    
    #form the exog for each nominee you want to determine odds for
    for nominee in nominees:
        nom_exog = [nominee['category_nominations'], nominee['nominee']['entrant_company_category_nominations']]
        total_title = 0
        total_company = 0
        total_wins = 0
        company_noms = 0
        company_wins = 0
        sum_company_wins = 0
        for cat in supporting_list_of_categories_without_results:
            sup_docs = shortlist_col.find_one({'year':nominee['year'], 'category':cat, 'nominee.entrant_company':nominee['nominee']['entrant_company'],'nominee.title': nominee['nominee']['title']})
            # company_noms += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}).count()
            for doc in shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}):
                company_noms += 1
                sum_company_wins += doc['category_nominations']
            if sup_docs:
                # print 'found ', sup_docs
                total_title += sup_docs['category_nominations']
                total_company += sup_docs['nominee']['entrant_company_category_nominations']
                nom_exog = nom_exog + [sup_docs['category_nominations'], sup_docs['nominee']['entrant_company_category_nominations']]
                
            else:
                nom_exog = nom_exog + [0,0]

        for cat in supporting_list_of_categories_with_results:
            sup_docs = shortlist_col.find_one({'year':nominee['year'], 'category':cat, 'nominee.entrant_company':nominee['nominee']['entrant_company'],'nominee.title': nominee['nominee']['title']})
            # company_noms += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}).count()
            company_wins += shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company'], 'result':True}).count()
            for doc in shortlist_col.find({'year':nominee['year'], 'category':cat, 'nominee.entrant_company': nominee['nominee']['entrant_company']}):
                company_noms += 1
                sum_company_wins += doc['category_nominations']
            if sup_docs:
                # print 'found ', sup_docs
                total_title += sup_docs['category_nominations']
                total_company += sup_docs['nominee']['entrant_company_category_nominations']
                _res = 1 if sup_docs['result'] else  0
                total_wins += _res
                nom_exog = nom_exog + [sup_docs['category_nominations'], sup_docs['nominee']['entrant_company_category_nominations'], _res]
                
            else:
                nom_exog = nom_exog + [0,0,0]
        nom_exog = nom_exog + [total_title, total_company, total_wins, company_noms, company_wins, sum_company_wins]
        predict_nominees.append([nominee, nom_exog, []])
    
    #determine the odds for each nominee
    enriched_noms = odds(res[0], res[1], res[2], res[3], predict_nominees)
    
    #throw odds into db or print to screen
    for nom in enriched_noms:
        # print nom[0]
        predictions_col.insert(nom[0])
        # predictions_col.update({'year': nom[0]['year'], 'category': nom[0]['category'], 
            # 'nominee.entrant_company':nom[0]['nominee']['entrant_company'],'nominee.title':nom[0]['nominee']['title']}, nom[0])

main()

