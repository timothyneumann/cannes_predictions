from pymongo import MongoClient
from operator import itemgetter
shortlist_col = MongoClient('localhost')['app-nest-cannes']['shortlist']
winners = shortlist_col.find({'result':True})
res = []
for winner in winners:
	res.append([winner['category'], winner['year'], shortlist_col.find({'year':winner['year'],  'nominee.entrant_company': winner['nominee']['entrant_company'],'nominee.title': winner['nominee']['title']}).count()])

res = sorted(res, key=itemgetter(2))
print res